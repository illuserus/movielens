//scalaVersion := "2.12.8"
name := "ML-100k"
version := "1.0"

val sparkVersion = "2.4.3"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  "mysql" % "mysql-connector-java" % "5.1.12"
//  "org.scalanlp" %% "breeze" % "0.12",
//  "org.scalanlp" %% "breeze-natives" % "0.12",
//  "org.scalanlp" %% "breeze-viz" % "0.12",
//  "org.scalanlp" % "breeze_2.11" % "0.12",
//  "org.scalanlp" % "breeze-viz_2.11" % "0.12"
)


//resolvers += "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"
