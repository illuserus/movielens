class Util extends Serializable{
    def convertYear( x:String) : Int = {
    try
      return x.takeRight(4).toInt
    catch {
      case e: Exception => println("exception caught: " + e + " Returning 1900");
        return 1900
    }
  }

  def convertDate(x:String) : Array[Int] = {
    val pat = """^(?<day>\d{2})\-(?<month>\S{3})\-(?<year>\d{4})$""".r
    return x match
    {
      case pat(day,month,year)=>Array(year.toInt,this.convertMonth(month),this.convertWeek(day.toInt))
      case _=>Array[Int]()
    }
  }
  
  def convertMonth(x:String) : Int = x match{
        case "Jan" => 1
        case "Feb" => 2
        case "Mar" => 3
        case "Apr" => 4
        case "May" => 5
        case "Jun" => 6
        case "Jul" => 7
        case "Aug" => 8
        case "Sep" => 9
        case "Oct" => 10
        case "Nov" => 11
        case "Dec" => 12
        case _ => 0
  }

  def convertWeek(x:Int) : Int = x/10+1;

  def convertGender(x:String) : Int = x match{
    case "M" => 1
    case "F" => 2
    case _ => 0
  }

  def convertAge(x:Int) :Int = x/10;

  def convertRating(x:Int) : Int = x-3;
    
  
}