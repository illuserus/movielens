import org.apache.spark.ml.fpm.FPGrowth
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._


class TrainingByFPG(data : RawData){
    val spark = SparkSession.builder.getOrCreate()
    import spark.implicits._
    
    val rawData :RawData = data;
    val DF = rawData.Rating
        .map{_.toSeq}
        .toDF("pattern");
    val fpg = new FPGrowth()
        .setItemsCol("pattern")
        .setMinSupport(0.5)
        .setMinConfidence(0.6)
    val model = fpg.fit(DF);
}