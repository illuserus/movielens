import org.apache.spark.ml.recommendation.ALS
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

class TrainingByALS(data : RawData,ratio : Array[Double]){
    val spark = SparkSession.builder.getOrCreate()
    import spark.implicits._
    val rawData :RawData = data;
    val als : ALS = {
        new ALS()                
            .setMaxIter(5)
            .setRegParam(0.01)
            .setUserCol("user_id")
            .setItemCol("movie_id")
            .setRatingCol("rating")
    }
    val Array(train,test) = this.rawData.Rating.randomSplit(ratio)
    val model = als.fit(train)
    val predictions = model.transform(this.rawData.Rating).na.drop()
    val movieRecs = model.recommendForAllItems(10)
    val userRecs = model.recommendForAllUsers(10)
    val conv_RatingDF = userRecs.select(
      col("user_id"),
      explode(col("recommendations")).as("recommand")
    )
    .map(r=>{
      CRating(
        r.getInt(0),
        r.getStruct(1).getInt(0),
        r.getStruct(1).getFloat(1)
    )})
    .toDF();
}