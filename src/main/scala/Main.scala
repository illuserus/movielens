import org.apache.spark._
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.mllib.evaluation.RegressionMetrics

import org.apache.spark.ml.recommendation.ALS

import org.apache.log4j.Logger
import org.apache.log4j.Level

import java.util.Properties
import java.sql.{Connection,DriverManager}


case class prediction(user_id:Int,movie_id:Int,rating:Int,prediction:Float);
case class CRating(user_id:Int,movie_id:Int,rating:Float);
object ML_100k
{
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  val conf : SparkConf = initSparkConf();
  val spark :SparkSession = SparkSession.builder().config(conf).getOrCreate()
      import spark.implicits._
  val DB :DB = new DB();
  def initSparkConf(): SparkConf = new SparkConf().setAppName("ML-100k").setMaster("local");
  def initSparkSession() : SparkSession = SparkSession.builder().config(conf).getOrCreate()
  def main(args:Array[String]) : Unit = 
  {
    

    val dataset = new RawData(spark,DB);
    eraseData(dataset);
    // println("Training ...")
    // val ALSResult = new TrainingByALS(dataset,Array(0.7,0.3));

    //val ALSResult.predictionDF.createOrReplaceTempView("predictionDF");
    // val evalMatrixDF = ALSResult.predictions.na.drop().toDF();

    // val evalMatrixRDD = evalMatrixDF
    // .map{ case Row(user_id :Integer ,movie_id : Integer,rating :Integer,timestamp : Integer,prediction : Float)=>(rating.toDouble,prediction.toDouble)}.rdd;

    // val evaluator = new RegressionEvaluator()
    //   .setMetricName("rmse")
    //   .setLabelCol("rating")
    //   .setPredictionCol("prediction")
    // val rmse = evaluator.evaluate(ALSResult.predictions);

    // val FPGResult = new TrainingByFPG(dataset);
    spark.stop();
  }

  def eraseData( data:RawData ) = {
    println("Erasing Data ...")

    data.Rating.createOrReplaceTempView("ori_Rating");
    data.Movies.createOrReplaceTempView("ori_Movies");
    data.Users.createOrReplaceTempView("ori_Users");

    val tools : Util = new Util();    
    val cDate = spark.udf.register("cDate",tools.convertDate _);
    val cGender = spark.udf.register("cGender",tools.convertGender _);
    val cAge = spark.udf.register("cAge",tools.convertAge _);
    val cRating = spark.udf.register("cRating", tools.convertRating _);

    tools.convertAge(23)
    val cMoviesDF = data.Movies
      .withColumn("_Date",cDate(col("date")))
      .withColumn("year",col("_Date")(0))
      .withColumn("month",col("_Date")(1))
      .withColumn("week",col("_Date")(2))
      .drop(col("date"))
      .drop(col("_Date"))
      .toDF();

    
    val cUsersDF = data.Users
      .withColumn("_age",cAge(col("age")))
      .withColumn("_gender",cGender(col("gender")))
      .drop(col("age"))
      .drop(col("gender"))
      .withColumnRenamed("_age","age")
      .withColumnRenamed("_gender","gender")
      .toDF();
    
    // val cRatingDF = data.Rating
    //   .withColumn("_rating",cRating(col("rating")))
    //   .drop(col("rating"))
    //   .withColumnRenamed("_rating","rating")
    //   .toDF();
    
    val cRatingDF = spark.sql("""select user_id,movie_id , cRating(rating) as rating from ori_Rating""")
    cRatingDF.show();

    cRatingDF.write.mode("overwrite").jdbc(DB.MYSQL_URL,"conv_Rating",DB.prop);
    // cUsersDF.write.mode("overwrite").jdbc(DB.MYSQL_URL,"conv_Users",DB.prop);
    // cMoviesDF.write.mode("overwrite").jdbc(DB.MYSQL_URL,"conv_Movies",DB.prop);
    // cRatingDF.write.mode("overwrite").jdbc();


  }

  // def traningByALS() : DataFrame= {
  //   println("Start ALS Traning ...");
  //   val Array(train,test) = RatingDF.randomSplit(Array(0.7,0.3))
  //   val als = new ALS()
  //               .setMaxIter(5)
  //               .setRegParam(0.01)
  //               .setUserCol("user_id")
  //               .setItemCol("movie_id")
  //               .setRatingCol("rating")

  //   val model = als.fit(train)
  //   val predictions = model.transform(RatingDF)
  //   //val movieRecs = model.recommendForAllItems(10)
  //   val userRecs = model.recommendForAllUsers(10)
  //   return predictions;
  //   // val conv_RatingDF = userRecs.select(
  //   //   col("user_id"),
  //   //   explode(col("recommendations")).as("recommand")
  //   // )
  //   // .map(r=>{
  //   //   CRating(
  //   //     r.getInt(0),
  //   //     r.getStruct(1).getInt(0),
  //   //     r.getStruct(1).getFloat(1)
  //   // )})
  //   // .toDF();
  //   // return conv_RatingDF;
  // }

  def toDB( df : DataFrame,mode:String,table:String) = {
      df.write.mode(mode).jdbc(DB.MYSQL_URL,table,DB.prop)
  }
}
