import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.log4j.Logger
import org.apache.log4j.Level

class RawData(val session:SparkSession,val DBinfo : DB){
    protected val _DB  = DBinfo;
    protected val _session = session;
    var Users = this.userLoadFromFile();
    var Rating = this.ratingLoadFromFile();
    var Movies = this.moviesLoadFromFile();

    def init() = {
        this.Users = loadDFFromDB("ori_Users");
        this.Rating = loadDFFromDB("ori_Rating");
        this.Movies = loadDFFromDB("ori_Movies");

        if(this.Users.count()==0){
            this.Users = userLoadFromFile();
        }

        if(this.Rating.count()==0){
            this.Rating = ratingLoadFromFile();
        }

        if(this.Movies.count()==0){
            this.Movies = moviesLoadFromFile();
        }
    }
    def userLoadFromFile() : DataFrame = {
        println("Loading Users data...")
        val schema = StructType(Array(
            StructField("id", IntegerType, false),
            StructField("age", StringType, false),
            StructField("gender", StringType, false),
            StructField("occupation", StringType, false),
            StructField("zipCode", StringType, false)
        ));
        val filename = "/home/illus/Code/scala/ml-100k/data/u.user";
        return this.loadDFFromFile(filename,schema,"|");
    }

    def ratingLoadFromFile() : DataFrame = {
        println("Loading Rating data...")
        val schema = StructType(Array(
          StructField("user_id",IntegerType,false),
          StructField("movie_id",IntegerType,false),
          StructField("rating",IntegerType,false),
          StructField("timestamp",IntegerType,false)
        ));
        val filename = "/home/illus/Code/scala/ml-100k/data/u.data";
        return this.loadDFFromFile(filename,schema,"\t");
    }
    def moviesLoadFromFile() : DataFrame = {
        println("Loading Movies data...")
        val schema = StructType(Array(
          StructField("id",IntegerType,false),
          StructField("name",StringType,false),
          StructField("date",StringType,false),
          StructField("unknow",IntegerType,false),
          StructField("url",StringType,false),
          StructField("unknow_tag",IntegerType,false),
          StructField("action",IntegerType,false),
          StructField("adventure",IntegerType,false),
          StructField("animation",IntegerType,false),
          StructField("childrens",IntegerType,false),
          StructField("comedy",IntegerType,false),
          StructField("crime",IntegerType,false),
          StructField("documentary",IntegerType,false),
          StructField("drama",IntegerType,false),
          StructField("fantasy",IntegerType,false),
          StructField("film-noir",IntegerType,false),
          StructField("horror",IntegerType,false),
          StructField("musical",IntegerType,false),
          StructField("romance",IntegerType,false),
          StructField("sci-fi",IntegerType,false),
          StructField("thriller",IntegerType,false),
          StructField("war",IntegerType,false),
          StructField("western",IntegerType,false)
        ));

        val filename = "/home/illus/Code/scala/ml-100k/data/u.item";
        return this.loadDFFromFile(filename,schema,"|");
    }
    def loadDFFromFile(filename : String , schema : StructType , delimiter : String = "|") : DataFrame = {
        return session.read
            .format("com.databricks.spark.csv")
            .option("delimiter",delimiter)
            .schema(schema)
            .load(filename);

    }
    def loadDFFromDB(table:String) : DataFrame = {
        this._session.read.jdbc(this._DB.MYSQL_URL,table,this._DB.prop);
    }

    def saveToDB(DF : DataFrame,table:String,mode:String = "overwrite") = {
        DF.write.mode(mode).jdbc(this._DB.MYSQL_URL,table,this._DB.prop);
    }

}
